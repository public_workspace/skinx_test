import React from 'react';

import './App.css';
import 'react-quill/dist/quill.snow.css';
import { Route, Routes ,BrowserRouter as Router,} from 'react-router-dom';
import NavbarComponent from './component/navbar';
import PostPagesComponent from './pages/postPages';
import TagPagesComponent from './pages/tagPages';
import PostDetailPagesComponent from './pages/postDetailPages';
import LoginAndRegisterPagesComponent from './pages/loginAndRegisterPage';

function App() {
  
  return (
    <Router>
      <div className='vh-100 d-flex flex-column'>
        <NavbarComponent />
        <div className="container py-3 flex-grow-1">
          <Routes>

             {/* Public routes */}
             <Route path="/login" element={<LoginAndRegisterPagesComponent />} />

             {/* Protected routes */}
            <Route path="/" element={<PostPagesComponent />} />
            <Route path="/tag" element={<TagPagesComponent />} />
            <Route path="/post/:id" element={<PostDetailPagesComponent />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
