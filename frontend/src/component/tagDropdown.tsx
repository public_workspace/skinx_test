import {
    OutlinedInput,
    InputLabel,
    MenuItem,
    Select,
    FormControl,
    Stack,
    Chip,
    SelectChangeEvent
} from "@mui/material";
import { useEffect, useState } from 'react';

interface Tag {
    id: number;
    name: string;
}

interface TagDropdownProps {
    selectedOptions: Tag[];
    setSelectedOptions: React.Dispatch<React.SetStateAction<Tag[]>>;
}

const TagDropdownComponent: React.FC<TagDropdownProps> = ({ selectedOptions, setSelectedOptions }) => {
    const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT;
    const [options, setOptions] = useState<Tag[]>([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`${API_ENDPOINT}/tag`);
                const data = await response.json();
                setOptions(data);
            } catch (error) {
                console.error('Error fetching options:', error);
            }
        };
        fetchData();
    }, [API_ENDPOINT]);

    const handleChange = (e: SelectChangeEvent<number[]>) => {
        const selectedIds = e.target.value as number[];
        const selectedTags = selectedIds.map(id => options.find(option => option.id === id)!);
        setSelectedOptions(selectedTags);
    };

    return (
        <FormControl className='w-100'>
            <InputLabel>Search Tag</InputLabel>
            <Select
                multiple
                value={selectedOptions.map(tag => tag.id)}
                onChange={handleChange}
                input={<OutlinedInput label="Search Tag" />}
                renderValue={(selected) => (
                    <Stack gap={1} direction="row" flexWrap="wrap">
                        {(selectedOptions as Tag[]).map((tag) => (
                            <Chip key={tag.id} label={tag.name} />
                        ))}
                    </Stack>
                )}
            >
                {options.map((tag) => (
                    <MenuItem key={tag.id} value={tag.id}>
                        {tag.name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
};

export default TagDropdownComponent;
