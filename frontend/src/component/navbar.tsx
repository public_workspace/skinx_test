import { Link } from 'react-router-dom';

const NavbarComponent = () => {

    const handleLogout = () => {
        localStorage.removeItem('token');
        window.location.href = '/login'
      };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container">
        <Link className="navbar-brand" to="/">SKINX</Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <Link className="nav-link" to="/">POST</Link>
            <Link className="nav-link" to="/tag">TAG</Link>
            <button className="nav-link btn btn-link" onClick={handleLogout}>LOGOUT</button>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default NavbarComponent;
