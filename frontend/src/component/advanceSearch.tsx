import Button from '@mui/material/Button';
import { TextField } from "@mui/material";
import { useState } from 'react';
import TagDropdownComponent from './tagDropdown';

interface Tag {
    id: number;
    name: string;
}

interface SearchData {
    searchText: string;
    selectedTags: Tag[];
}

interface AdvanceSearchProps {
    onSearch: (data: SearchData) => void;
}

const AdvanceSearchComponent: React.FC<AdvanceSearchProps> = ({ onSearch }) => {
    const [searchText, setSearchText] = useState('');
    const [selectedTags, setSelectedTags] = useState<Tag[]>([]);

    const handleSearch = () => {
        const searchData: SearchData = {
            searchText,
            selectedTags
        };
        onSearch(searchData);
    };

    return (
        <div className="card p-2">
            <h4>Filter Post by your settings</h4>
            <div className="row">
                <div className="col-md-6">
                    <TextField
                        className='w-100'
                        id="outlined-basic"
                        label="Search Text"
                        variant="outlined"
                        value={searchText}
                        onChange={(e) => setSearchText(e.target.value)}
                    />
                </div>
                <div className="col-md-6">
                    <TagDropdownComponent
                        selectedOptions={selectedTags}
                        setSelectedOptions={setSelectedTags}
                    />
                </div>
                <div className="col-12 mt-2">
                    <Button className='w-100' variant="contained" onClick={handleSearch}>
                        Search
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default AdvanceSearchComponent;
