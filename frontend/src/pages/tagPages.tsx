import React, { useEffect, useState } from 'react';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@mui/material';
import ConfirmationModal from '../component/confirmModal';
import { useForm } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';

interface Tag {
    id: number;
    name: string;
}

const TagPagesComponent = () => {

    const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT || 'http://backend:3000';
    const [tags, setTags] = useState<Tag[]>([]); 
    const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
    const [tagToDeleteId, setTagToDeleteId] = useState<number | null>(null);

    const [createModalOpen, setCreateModalOpen] = useState(false);
    const { register, handleSubmit,reset, formState: { errors } } = useForm();


    const fetchTags = async (API_ENDPOINT: string, setTags: React.Dispatch<React.SetStateAction<Tag[]>>) => {
        try {
            const response = await fetch(`${API_ENDPOINT}/tag` , {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const data = await response.json();
            setTags(data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {

        const token = localStorage.getItem('token');
        if (!token) {
            const redirectToLogin = () => {
                window.location.href = '/login';
            };
            redirectToLogin();
        }

        fetchTags(API_ENDPOINT, setTags);
    }, [API_ENDPOINT]);

    const handleConfirmDelete = async () => {
        try {
            const response = await fetch(`${API_ENDPOINT}/tag/${tagToDeleteId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });
            if (!response.ok) {
                throw new Error('Failed to delete tag');
            }else{
                alert('delte - success')
            }
            fetchTags(API_ENDPOINT, setTags);
        } catch (error) {
            console.error('Error deleting tag:', error);
        }
        setDeleteConfirmationOpen(false);
    };


    const handleCreateTag = async (formData: any) => {
        try {
            const response = await fetch(`${API_ENDPOINT}/tag`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({ name: formData.name }),
            });
            if (!response.ok) {
                throw new Error('Failed to create tag');
            }else{
                alert('create - success')
            }
           
            setCreateModalOpen(false);
            reset(); 
            fetchTags(API_ENDPOINT, setTags);
        } catch (error) {
            console.error('Error creating tag:', error);
        }
    };

    const handleDeleteClick = (tagId: number) => {
        setTagToDeleteId(tagId);
        setDeleteConfirmationOpen(true);
    };

    const columns: GridColDef[] = [
        { field: 'id', headerName: 'ID', width: 90 },
        { field: 'name', headerName: 'Name', flex: 1 },
        {
            field: 'actions',
            headerName: 'Actions',
            width: 150,
            renderCell: (params) => (
                <Button onClick={() => handleDeleteClick(params.row.id)}>DEL</Button>
            ),
        },
    ];


    const handleCreateClick = () => {
        setCreateModalOpen(true);
    };
    const handleCreateModalClose = () => {
        setCreateModalOpen(false);
    };

    return (
        <div className="container py-2 h-100">
            <div className='my-4' style={{ width: '100%' }}>
                <Button onClick={handleCreateClick} >Create Tag</Button>
                <DataGrid rows={tags} columns={columns} />
            </div>


            <ConfirmationModal
                open={deleteConfirmationOpen}
                onClose={() => setDeleteConfirmationOpen(false)}
                onConfirm={handleConfirmDelete}
                title="Confirm Deletion"
                message="Are you sure you want to delete this tag?"
            />

            <Dialog open={createModalOpen} onClose={handleCreateModalClose}>
                <DialogTitle>Create New Tag</DialogTitle>
                <DialogContent>
                    <form onSubmit={handleSubmit(handleCreateTag)}>
                        <div className='mb-3 text-start'>
                                <label className="form-label w-100">Name</label>
                                <input
                                    className='form-control'
                                    {...register("name", { required: "This is required." })}
                                />
                                <ErrorMessage errors={errors} name="name" />
                        </div>
                        <DialogActions>
                            <Button onClick={handleCreateModalClose}>Cancel</Button>
                            <Button type="submit">Create</Button>
                        </DialogActions>
                    </form>
                </DialogContent>
            </Dialog>

        </div>
    );
};

export default TagPagesComponent;
