import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

interface PostDetail {
    id:number ,
    title:string ,
    content:string ,
    postedBy: User
    tags: Tag[];
}

interface User {
    id: number ;
    username: string ;
}

interface Tag {
    id: number;
    name: string;
}

const PostDetailPagesComponent = () => {
    const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT || 'http://backend:3000';
    const { id } = useParams<{ id: string }>(); 
    const [post, setPost] = useState<PostDetail | null>(null);
  
    useEffect(() => {

        const token = localStorage.getItem('token');
        if (!token) {
            const redirectToLogin = () => {
                window.location.href = '/login';
            };
            redirectToLogin();
        }

        const fetchPostDetails = async () => {
            try {
                const response = await fetch(`${API_ENDPOINT}/post/${id}` ,  {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
                if (!response.ok) {
                    throw new Error('Failed to fetch post details');
                }
                const postData = await response.json();
                setPost(postData);
            } catch (error) {
                console.error('Error fetching post details:', error);
            }
        };
        fetchPostDetails();
    }, [id, API_ENDPOINT]);
  
    if (!post) {
        return <div>Loading...</div>;
    }
  
    return (
        <div>
            <div className='w-100 py-2'>
                <a className='text-danger' href="/">RETURN</a>
            </div>
            <div className='d-flex justify-content-between'>
                <h5>{post.postedBy.username}</h5>
                <div>
                    {post.tags.map(tag => (
                        <div className='card px-2 mx-1' key={tag.id}>{tag.name}</div>
                    ))}
                </div>
            </div>
            <div className='card p-2'>
                <h2>{post.title}</h2>
                <div dangerouslySetInnerHTML={{ __html: post.content }} />
            </div>
        </div>
    );
};

export default PostDetailPagesComponent;


