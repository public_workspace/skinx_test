import React, { useEffect, useState } from 'react';
import AdvanceSearchComponent from '../component/advanceSearch';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { ErrorMessage } from '@hookform/error-message';
import { useForm } from 'react-hook-form';
import ReactQuill from 'react-quill';
import TagDropdownComponent from '../component/tagDropdown';
import ConfirmationModal from '../component/confirmModal';

interface Post {
    id: number;
    title: string;
    content: string;
    postedAt: string;
    postedBy: User
    tags: Tag[];
}

interface User {
    id: number ;
    username: string ;
}

interface Tag {
    id: number;
    name: string;
}

interface SearchData {
    searchText: string;
    selectedTags: Tag[];
}

interface SearchModel {
    title : string;
    tagIds : number[];

}

const PostPagesComponent = () => {
    const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT || 'http://backend:3000';
    const [posts, setPosts] = useState<Post[]>([]);

    const [createModalOpen, setCreateModalOpen] = useState(false);
    const { register, handleSubmit,reset, formState: { errors } } = useForm();
    const [content, setContent] = useState('');
    const [contentError, setContentError] = useState('');
    const [searchModel, setSearchModel] = useState<SearchModel>({
        title : '',
        tagIds : []
    })


    const [postToDeleteId, setPostToDeleteId] = useState<number | null>(null);
    const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
    const [tag, setTag] = useState<Tag[]>([]);

    const fetchPosts = async () => {
        try {

            const response = await fetch(`${API_ENDPOINT}/post?title=${searchModel.title}&tagIds=${searchModel.tagIds}` , {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const data = await response.json();
            setPosts(data.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };


    const handleConfirmDelete = async () => {
        try {
            const response = await fetch(`${API_ENDPOINT}/post/${postToDeleteId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });
            if (!response.ok) {
                throw new Error('Failed to delete tag');
            }else{
                alert('delte - success')
            }
            fetchPosts();
        } catch (error) {
            console.error('Error deleting tag:', error);
        }
        setDeleteConfirmationOpen(false);
    };

    useEffect(() => {

        const token = localStorage.getItem('token');
        if (!token) {
            const redirectToLogin = () => {
                window.location.href = '/login';
            };
            redirectToLogin();
        }
    

        fetchPosts();
    }, [API_ENDPOINT]);

    const handleDetailClick = (postId: number) => {
        window.location.href = `/post/${postId}`;
    };

    const handleDeleteClick = (tagId: number) => {
        setPostToDeleteId(tagId);
        setDeleteConfirmationOpen(true);
    };

    const columns : GridColDef[] = [
        { field: 'id', headerName: 'ID', width: 90 },
        { field: 'title', headerName: 'Title', flex: 1 },
        { field: 'postedAt', headerName: 'Posted At', flex: 1 },
        { field: 'postedBy', headerName: 'Posted By', flex: 1 },
        {
            field: 'actions',
            headerName: 'Actions',
            width: 300,
            renderCell: (params) => (
                <div>
                    <Button onClick={() => handleDetailClick(params.row.id)}>See Details</Button>
                    <Button className='text-danger' onClick={() => handleDeleteClick(params.row.id)}>DEL</Button>
                </div>
            ),
        },
    ];

    const rows = posts.map((post) => ({
        id: post.id,
        title: post.title,
        postedAt: post.postedAt,
        postedBy: post.postedBy.username,
        tags: post.tags.map(tag => tag.name).join(', '),
      }));

      const handleCreatePost = async (formData: any) => {
        try {

            if (!content) {
                setContentError('Content is required');
                return;
            }else{
                setContentError('');
            }
    
            const response = await fetch(`${API_ENDPOINT}/post`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {   
                        content : content ,
                        title : formData.title ,
                        tags : tag
                    }
                    ),
            });
            if (!response.ok) {
                throw new Error('Failed to create post');
            } else {
                alert('Post created successfully');
            }
    
            setCreateModalOpen(false);
            reset();
            fetchPosts();
        } catch (error) {
            console.error('Error creating post:', error);
        }
    };

    const handleCreateClick = () => {
        setCreateModalOpen(true);
    };
    const handleCreateModalClose = () => {
        setCreateModalOpen(false);
    };

    const handleSearch = async (searchData: SearchData) => {
        try {
            const tagIds = searchData.selectedTags.map(tag => tag.id);
            const params = {
                title: searchData.searchText,
                tagIds: tagIds 
            };
            setSearchModel(params);
            await fetchPosts();
        } catch (error) {
            console.error('Error searching posts:', error);
        }
    };
  return (
    <div className="container py-2 h-100">
         <Button onClick={handleCreateClick} >Create Post</Button>
        <AdvanceSearchComponent onSearch={handleSearch} />
        <div className='my-4' style={{ width: '100%' }}>
            <DataGrid rows={rows} columns={columns} />
        </div>

        <ConfirmationModal
                open={deleteConfirmationOpen}
                onClose={() => setDeleteConfirmationOpen(false)}
                onConfirm={handleConfirmDelete}
                title="Confirm Deletion"
                message="Are you sure you want to delete this post?"
            />


        <Dialog open={createModalOpen} onClose={handleCreateModalClose}>
                <DialogTitle>Create New Tag</DialogTitle>
                <DialogContent>
                    <form onSubmit={handleSubmit(handleCreatePost)}>
                        <div className='mb-3 text-start'>
                                <label className="form-label w-100">title</label>
                                <input
                                    className='form-control mb-2'
                                    {...register("title", { required: "This is required." })}
                                />
                                <ErrorMessage errors={errors} name="title" />
                        </div>
                        <div className='mb-3 text-start'>
                            <label className="form-label w-100">Content</label>
                            <ReactQuill
                                theme="snow"
                                value={content}
                                onChange={setContent}
                            />
                                {contentError && <div className='mt-2'>{contentError}</div>}
                        </div>
                        <div className='mb-3 text-start'>
                            <label className="form-label w-100">Content</label>
                            <TagDropdownComponent selectedOptions={tag} setSelectedOptions={setTag} />
                        </div>
                        <DialogActions>
                            <Button onClick={handleCreateModalClose}>Cancel</Button>
                            <Button type="submit">Create</Button>
                        </DialogActions>
                    </form>
                </DialogContent>
        </Dialog>
    </div>
  );
};

export default PostPagesComponent;


