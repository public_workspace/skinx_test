import { useState } from 'react';

const LoginAndRegisterPagesComponent = () => {
    const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT || 'http://backend:3000';
    const [loginUsername, setLoginUsername] = useState('');
    const [loginPassword, setLoginPassword] = useState('');
    const [registerUsername, setRegisterUsername] = useState('');
    const [registerPassword, setRegisterPassword] = useState('');
    const [error, setError] = useState(null);

    const handleLogin = async () => {
        try {
            const response = await fetch(`${API_ENDPOINT}/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ username: loginUsername, password: loginPassword }),
            });

            if (!response.ok) {
                throw new Error('Invalid credentials');
            }

            const data = await response.json();
            localStorage.setItem('token', data.token);
            window.location.href = '/'
        } catch (error : any) {
            setError(error.message);
        }
    };

    const handleRegister = async () => {
        try {
            const response = await fetch(`${API_ENDPOINT}/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ username: registerUsername, password: registerPassword }),
            });

            if (!response.ok) {
                throw new Error('Registration failed');
            }

            await handleLogin();
        } catch (error : any) {
            setError(error.message);
        }
    };

    const resetLoginFields = () => {
        setLoginUsername('');
        setLoginPassword('');
    };

    const resetRegisterFields = () => {
        setRegisterUsername('');
        setRegisterPassword('');
    };

    return (
        <div>
            <ul className="nav nav-tabs" id="myTab" role="tablist">
                <li className="nav-item" role="presentation">
                    <button onClick={() => resetLoginFields()} className="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Login</button>
                </li>
                <li className="nav-item" role="presentation">
                    <button onClick={() => resetRegisterFields()} className="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Register</button>
                </li>
            </ul>
            <div className="tab-content" id="myTabContent">
                <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div className='d-flex flex-column'>
                        {error && <div>{error}</div>}
                        <input
                            className='my-2'
                            type="text"
                            placeholder="Username"
                            value={loginUsername}
                            onChange={(e) => setLoginUsername(e.target.value)}
                        />
                        <input
                            className='mb-2'
                            type="password"
                            placeholder="Password"
                            value={loginPassword}
                            onChange={(e) => setLoginPassword(e.target.value)}
                        />
                        <button className='btn btn-primary' onClick={() => { handleLogin(); resetLoginFields(); }}>Login</button>
                    </div>
                </div>
                <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div className='d-flex flex-column'>
                        {error && <div>{error}</div>}
                        <input
                            className='my-2'
                            type="text"
                            placeholder="Username"
                            value={registerUsername}
                            onChange={(e) => setRegisterUsername(e.target.value)}
                        />
                        <input
                            className='mb-2'
                            type="password"
                            placeholder="Password"
                            value={registerPassword}
                            onChange={(e) => setRegisterPassword(e.target.value)}
                        />
                        <button className='btn btn-primary' onClick={() => { handleRegister(); resetRegisterFields(); }}>Register</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LoginAndRegisterPagesComponent;
