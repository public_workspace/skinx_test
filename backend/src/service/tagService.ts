import { DataSourceConfig } from "../config/databaseConfig";
import { TagDto } from "../dto/tagDto";
import { Tag } from "../entity/tagEntity";

export class TagService {
    private tagRepository = DataSourceConfig.getRepository(Tag);

    async create(createTagDto: TagDto): Promise<Tag> {
        const { name } = createTagDto;
        const tag = new Tag();
        tag.name = name;
        return await this.tagRepository.save(tag);
    }

    async findAll(): Promise<Tag[]> {
        return await this.tagRepository.find();
    }

    async delete(id: number): Promise<boolean> {
        const tagToDelete = await this.tagRepository.findOneBy({ id });
        if (!tagToDelete) return false;

        await this.tagRepository.delete(id);
        return true;
    }

}