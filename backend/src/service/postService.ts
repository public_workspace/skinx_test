import { In, Like } from "typeorm";
import { DataSourceConfig } from "../config/databaseConfig";
import { PostDto, PostSearchParamsDto } from "../dto/postDto";
import { Post } from "../entity/postEntity";

export class PostService {
    private postRepository = DataSourceConfig.getRepository(Post);

    async create(createPostDto: PostDto): Promise<Post> {
        const { title, content, postedBy, tags } = createPostDto;
        const post = new Post();
        post.title = title;
        post.content = content;
        post.postedBy = postedBy;
        post.tags = tags;

        return await this.postRepository.save(createPostDto);
    }

    async findAll(searchParamsDto: PostSearchParamsDto): Promise<Post[]> {
        let queryOptions: any = { relations: ['tags', 'postedBy'] };

        if (searchParamsDto.title) {
            queryOptions.where = { title: Like(`%${searchParamsDto.title}%`) };
        }

        if (searchParamsDto.sortByPostedAt) {
            queryOptions.order = { postedAt: searchParamsDto.sortByPostedAt };
        }

        if (searchParamsDto.tagIds && searchParamsDto.tagIds.length > 0) {
            queryOptions.where = { tags: { id: In(searchParamsDto.tagIds) } };
        }

        return await this.postRepository.find(queryOptions);
    }

    async findById(postId: number): Promise<Post | null> {
        try {
            const post = await this.postRepository.findOne({
                where : {
                    id : postId
                },
                relations : {
                    tags : true,
                    postedBy : true
                }
            });
            return post || null;
        } catch (error) {
            console.error("Error finding post by ID:", error);
            return null; 
        }
    }

    async deleteById(postId: number): Promise<void> {
        try {
            const post = await this.postRepository.findOne({
                where : {
                    id : postId
                }
            });

            if (!post) {
                throw new Error('Post not found');
            }
            await this.postRepository.remove(post);
        } catch (error) {
            console.error("Error deleting post:", error);
            throw error;
        }
    }

    
}