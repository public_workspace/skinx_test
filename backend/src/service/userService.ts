import { User } from "../entity/userEntity";
import { UserDTO } from "../dto/userDto";
import * as bcrypt from "bcrypt";
import { DataSourceConfig } from "../config/databaseConfig";

export class UserService {
    private userRepository = DataSourceConfig.getRepository(User);

    async register(userDTO: UserDTO): Promise<void> {
        const hashedPassword = await bcrypt.hash(userDTO.password, 10);
        const user = this.userRepository.create({
            username: userDTO.username,
            password: hashedPassword
        });
        await this.userRepository.save(user);
    }

    async findByUsername(username: string): Promise<User | undefined> {
        const user = await this.userRepository.findOne({ where: { username } });
        return user || undefined;
    }

    async comparePasswords(candidatePassword: string, hashedPassword: string): Promise<boolean> {
        return await bcrypt.compare(candidatePassword, hashedPassword);
    }

}