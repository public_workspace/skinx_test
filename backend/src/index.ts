import "reflect-metadata";
import { DataSourceConfig } from './config/databaseConfig';
import jwt from 'jsonwebtoken';

import express, { Application,NextFunction,Request, Response } from 'express';
import bodyParser from "body-parser";
import dotenv from 'dotenv';
import cors from 'cors';
dotenv.config(); 

// controller
import { userController } from "./controller/userController";
import { tagController } from "./controller/tagController";
import { postController } from "./controller/postController";

const app: Application = express();
const PORT: number | string = process.env.PORT || 3000;

const JWT_SECRET = 'JWT';
import { Request as ExpressRequest } from 'express';
interface CustomRequest extends ExpressRequest {
  decodedToken?: { userId: string }; // Define decodedToken property
}

const checkAccessToken = (req: CustomRequest, res: Response, next: NextFunction) => {
  const authorizationHeader = req.headers['authorization'];
  
  if (!authorizationHeader || !authorizationHeader.startsWith('Bearer ')) {
      return res.status(401).json({ message: 'Authorization header is missing or invalid' });
  }

  const token = authorizationHeader.split(' ')[1];

  if (!token) {
      return res.status(401).json({ message: 'Bearer token is missing' });
  }

  try {
      const decodedToken = jwt.verify(token, JWT_SECRET) as { userId: string };
      req.decodedToken = decodedToken;

      next();
  } catch (error) {
      console.error('Error verifying token:', error);
      return res.status(401).json({ message: 'Failed to authenticate token' });
  }
};


const startServer = async (): Promise<void> => {
  try {

    await DataSourceConfig
    .initialize()
    .then(() => {
        console.log("Data Source has been initialized!")
    })
    .catch((err : Error) => {
        console.error("Error during Data Source initialization:", err)
    })

    app.use(cors());
    app.use(express.json());
    app.use(bodyParser.json())

    app.use((req: Request, res: Response, next: NextFunction) => {
      if (req.path !== '/register' && req.path !== '/login') {
          checkAccessToken(req, res, next);
      } else {
          next();
      }
  });

  app.use((req: Request, res: Response, next: NextFunction) => {
      if (req.path !== '/register' && req.path !== '/login') {
          checkAccessToken(req, res, next);
      } else {
          next();
      }
  });

    // path
    // authentication
    app.post("/register", async (req: Request, res: Response) => {
      await userController.register(req, res);
    });
    app.post("/login", async (req: Request, res: Response) => {
      await userController.login(req, res);
    });
    // tag
    app.post("/tag", async (req: Request, res: Response) => {
      await tagController.create(req, res);
    });
    app.get("/tag", async (req: Request, res: Response) => {
      await tagController.getAll(req, res);
    });
    app.delete("/tag/:id", async (req: Request, res: Response) => {
      await tagController.delete(req, res);
    });
    // post
    app.post("/post", async (req: Request, res: Response) => {
      await postController.create(req, res);
    });
    app.get("/post", async (req: Request, res: Response) => {
      await postController.getAll(req, res);
    });
    app.get("/post/:id", async (req: Request, res: Response) => {
      await postController.getById(req, res);
    });
    app.delete("/post/:id", async (req: Request, res: Response) => {
      await postController.deleteById(req, res);
    });

    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });

  } catch (error : any) {
    console.error('Error starting the server :', error.message);
    process.exit(1); 
  }
};



startServer();