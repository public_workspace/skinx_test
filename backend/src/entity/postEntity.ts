import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, ManyToOne } from "typeorm";
import { Tag } from "./tagEntity";
import { User } from "./userEntity";

@Entity()
export class Post {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({nullable : false})
    title!: string;

    @Column({type: "longtext" ,nullable : false})
    content!: string;

    @Column({type: "timestamp",nullable : false , default: () => "CURRENT_TIMESTAMP" })
    postedAt!: string;

    @ManyToOne(() => User, user => user.posts)
    postedBy!: User;

    @ManyToMany(() => Tag, tag => tag.posts)
    @JoinTable()
    tags!: Tag[];

}
