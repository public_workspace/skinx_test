import { Entity, PrimaryGeneratedColumn, Column, OneToMany, Unique } from "typeorm";
import { Post } from "./postEntity";

@Entity()
@Unique(["username"])
export class User {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({nullable : false})
    username!: string;

    @Column({ type: "longtext",nullable : false})
    password!: string;

    @OneToMany(() => Post, post => post.postedBy)
    posts!: Post[];
}
