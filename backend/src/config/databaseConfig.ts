import { DataSource } from "typeorm"
import { Post } from "../entity/postEntity"
import { User } from "../entity/userEntity"
import { Tag } from "../entity/tagEntity"

export const DataSourceConfig = new DataSource({
    type: "mysql",
    host: "datastore",
    port: 3306,
    username: "root",
    password: "root",
    database: "store",
    entities: [Post,User,Tag],
    synchronize: true,
})