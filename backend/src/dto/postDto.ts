import { Tag } from "../entity/tagEntity";
import { User } from "../entity/userEntity";

export class PostDto {
    readonly title!: string;
    readonly content!: string;
    readonly postedBy!: User; 
    readonly tags!: Tag[];
}


export class PostSearchParamsDto {
    title?: string;
    sortByPostedAt?: 'ASC' | 'DESC';
    tagIds?: number[];
}