// userController.ts
import { Request, Response } from 'express';
import { UserService } from '../service/userService';
import { UserDTO } from '../dto/userDto';
import jwt from 'jsonwebtoken';

const userService = new UserService();
const JWT_SECRET = 'JWT';

export const userController = {
    async register(req: Request, res: Response): Promise<void> {
        try {
            const userDTO = req.body as UserDTO;
            await userService.register(userDTO);
            res.status(201).send("User registered successfully");
        } catch (error) {
            console.error(error);
            res.status(500).send("Error registering user");
        }
    },

    async login(req: Request, res: Response): Promise<void> {
        try {
            const { username, password } = req.body;
            const user = await userService.findByUsername(username);

            if (!user) {
                res.status(401).send("User not found");
                return;
            }

            const passwordsMatch = await userService.comparePasswords(password, user.password);
            
            if (!passwordsMatch) {
                res.status(401).json({ error: "Invalid password" });
                return;
            }

            // Generate JWT token
            const token = jwt.sign({ userId: user.id }, JWT_SECRET, { expiresIn: '1h' });

            // Send token as response
            res.status(200).json({ token });
        } catch (error) {
            console.error(error);
            res.status(500).send("Error logging in");
        }
    }
};
