// userController.ts
import { Request, Response } from 'express';
import { TagService } from '../service/tagService';
import { TagDto } from '../dto/tagDto';

const tagService = new TagService();

export const tagController = {
    async create(req: Request, res: Response): Promise<void> {
        try {
            const tagDTO = req.body as TagDto;
            await tagService.create(tagDTO);
            res.status(201).send("tag created successfully");
        } catch (error) {
            console.error(error);
            res.status(500).send("Error create tag");
        }
    },
    async getAll(req: Request, res: Response): Promise<void> {
        try {
            const tags = await tagService.findAll();
            res.status(200).json(tags);
        } catch (error) {
            console.error(error);
            res.status(500).send("Error fetching tags");
        }
    },
    async delete(req: Request, res: Response): Promise<void> {
        try {
            const { id } = req.params;
            const result = await tagService.delete(parseInt(id));
            if (!result) {
                res.status(404).send("Tag not found");
                return;
            }
            res.status(200).send("Tag deleted successfully");
        } catch (error) {
            console.error(error);
            res.status(500).send("Error deleting tag");
        }
    }
};
