// userController.ts
import { Request, Response } from 'express';
import { PostService } from '../service/postService';
import { PostDto, PostSearchParamsDto } from '../dto/postDto';

const postService = new PostService();

export const postController = {
    async create(req: Request, res: Response): Promise<void> {
        try {
            const postDTO = req.body as PostDto;
            await postService.create(postDTO);
            res.status(201).send("post created successfully");
        } catch (error) {
            console.error(error);
            res.status(500).send("Error create post");
        }
    },
    async getAll(req: Request, res: Response): Promise<void> {
        try {
            const { title, sortByPostedAt, tagIds } = req.query as unknown as PostSearchParamsDto;
            const posts = await postService.findAll({ title, sortByPostedAt, tagIds });
            let response = {
                data : posts ,
                total : posts.length
            }
            res.status(200).json(response);
        } catch (error) {
            console.error(error);
            res.status(500).send("Error retrieving posts");
        }
    },
    async getById(req: Request, res: Response): Promise<void> {
        try {
            const postId = parseInt(req.params.id);
            const post = await postService.findById(postId);
            if (post) {
                res.status(200).json(post);
            } else {
                res.status(404).send("Post not found");
            }
        } catch (error) {
            console.error(error);
            res.status(500).send("Error retrieving post");
        }
    },
    async deleteById(req: Request, res: Response): Promise<void> {
        try {
            const postId = parseInt(req.params.id);

            await postService.deleteById(postId);
            res.status(204).send();
        } catch (error) {
            console.error(error);
            res.status(500).send("Error deleting post");
        }
    }
};
